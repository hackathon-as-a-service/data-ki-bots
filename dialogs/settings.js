var builder = require("botbuilder");

module.exports = [
    (session, args, next) => {
        const msg = new builder.Message(session).addAttachment(
            settingsCard(session)
        );
        session.send(msg);
        session.endDialog();
    }
];

const settingsCard = session => {
    return new builder.HeroCard(session)
        .title("Settings")
        .text(`Name: ${session.userData.name}`)
        .buttons([
            builder.CardAction.imBack(
                session,
                "I would like to change my name",
                "Change Name"
            )
        ]);
};
