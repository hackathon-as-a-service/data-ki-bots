var builder = require("botbuilder");
var answers = require("../speech/answers");

const dialog = recognizer => {
    let dialog = new builder.IntentDialog({
        recognizers: [recognizer]
    });

    dialog.matches(/^vote:\[*.*\]/, "vote");

    dialog.matches(/^suggestions:\[*.*\]/, "suggestions");
    dialog.matches("continue", "continue");
    dialog.matches("vote", "vote");
    dialog.matches("settings", "settings");
    dialog.matches("delete", "delete");
    dialog.matches("changeName", "name");
    dialog.matches("learn", "learning");
    dialog.matches("greet", "greet");
    dialog.matches("None", [
        session => {
            session.send(
                answers.formatedRandomAnswer("unknown", [session.userData.name])
            );
        }
    ]);
    return dialog;
};

module.exports = { dialog };
