const builder = require("botbuilder");
const GSearch = require("../utils/gsearch");
const store = require("../store/suggestion");
var base64 = require("base-64");

module.exports = [
    (session, args, next) => {
        if (!/^suggestions:\[*.*\]/.test(session.message.text)) {
            const msg = suggCards(session, args);

            const choices = [];
            args.forEach(element => {
                choices.push(JSON.stringify(element));
            });
            builder.Prompts.choice(session, msg, choices);
            session.endDialog();
        } else {
            const args = store.getWithTopic(
                10,
                session.message.text
                    .replace(/^suggestions:\[/, "")
                    .replace(/\]/, ""),
                session.userData
            );
            const msg = suggCards(session, args);

            const choices = [];
            args.forEach(element => {
                choices.push(JSON.stringify(element));
            });
            builder.Prompts.choice(session, msg, choices);
            session.endDialog();
        }
    }
    //(session, results, next) => {
    //    session.replaceDialog("vote", JSON.parse(results.response.entity));
    //} vote : url
];

const suggCards = (session, args) => {
    let cards = [];
    args.forEach(element => {
        let img = GSearch.getImg(element.title);
        if (!img) {
            img =
                "https://www.google.com/s2/favicons?domain_url=" + element.url;
        }
        cards.push(suggCard(session, element, img));
    });

    return new builder.Message(session)
        .attachmentLayout(builder.AttachmentLayout.carousel)
        .attachments(cards);
};

const suggCard = (session, sugg, img) => {
    return new builder.ThumbnailCard(session)
        .title(sugg.title)
        .images([builder.CardImage.create(session, img)])
        .buttons([
            builder.CardAction.openUrl(
                session,
                `http://localhost:3978/redirect/${base64.encode(
                    JSON.stringify(session.logger.address)
                )}/${base64.encode(sugg.url)}`,
                "Open Website"
            ),
            builder.CardAction.postBack(
                session,
                "vote:[" + sugg.url + "]",
                "Rate Content"
            )
        ]);
};
