var builder = require("botbuilder");
var suggestionStore = require("../store/suggestion");
var answers = require("../speech/answers");

module.exports = [
    (session, args, next) => {
        if (/^vote:\[*.*\]/.test(session.message.text)) {
            var url = session.message.text
                .replace(/^vote:\[/, "")
                .replace(/\]$/, "");

            session.dialogData.suggestion = suggestionStore.getSuggestionByUrl(
                url,
                session.userData
            );
        } else {
            session.dialogData.suggestion = session.userData.currentArticle;
            delete session.userData.currentArticle;
        }

        if (!session.dialogData.suggestion) session.endDialog();

        const choices = ["Not for me", "It was ok", "Loved it"];
        const reply = new builder.Message(session).addAttachment(
            new builder.HeroCard(session)
                .text(
                    answers.formatedRandomAnswer("vote.question", [
                        session.userData.name,
                        session.dialogData.suggestion.title,
                        session.dialogData.suggestion.topic
                    ])
                )
                .buttons([
                    builder.CardAction.postBack(
                        session,
                        "Not for me",
                        "Not for me"
                    ),
                    builder.CardAction.postBack(
                        session,
                        "It was ok",
                        "It was ok"
                    ),
                    builder.CardAction.postBack(session, "Loved it", "Loved it")
                ])
        );

        builder.Prompts.choice(session, reply, choices);
    },
    (session, args, next) => {
        switch (args.response.index) {
            case 0: {
                session.send(
                    answers.formatedRandomAnswer("vote.bad", [
                        session.userData.name,
                        session.dialogData.suggestion.title,
                        session.dialogData.suggestion.topic
                    ])
                );
                break;
            }
            case 1: {
                session.send(
                    answers.formatedRandomAnswer("vote.ok", [
                        session.userData.name,
                        session.dialogData.suggestion.title,
                        session.dialogData.suggestion.topic
                    ])
                );
                break;
            }
            case 2: {
                session.send(
                    answers.formatedRandomAnswer("vote.awesome", [
                        session.userData.name,
                        session.dialogData.suggestion.title,
                        session.dialogData.suggestion.topic
                    ])
                );
                break;
            }
        }

        suggestionStore.setAnalytic(
            session.dialogData.suggestion.url,
            true,
            args.response.index,
            session.userData
        );

        session.userData.contextArticle = session.dialogData.suggestion;
        session.endDialog();
    }
];
