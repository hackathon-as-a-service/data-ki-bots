var builder = require("botbuilder");
var durationPrompt = require("../prompts/durationPrompt");
var entityPrompt = require("../prompts/topicPrompt");
var luisUtils = require("../utils/luis");
var client = require("../backend/client");
var suggestionStore = require("../store/suggestion");
var answers = require("../speech/answers");

const skillLevels = ["low", "medium", "high"];

module.exports = [
    function(session, args, next) {
        session.dialogData.skillLevel = getSkillLevel(args.entities);
        session.dialogData.topic = getTopic(args.entities);
        session.dialogData.duration = getDuration(args.entities);

        if (!session.dialogData.topic) {
            entityPrompt.prompt(
                session,
                answers.formatedRandomAnswer("learn.topic", [
                    session.userData.name
                ])
            );
        } else next();
    },

    function(session, args, next) {
        session.dialogData.topic = session.dialogData.topic || args.response;

        if (!session.dialogData.skillLevel) {
            const choices = [
                "I'm a beginner",
                "I have some experience",
                "I'm a pro"
            ];

            const reply = new builder.Message(session).addAttachment(
                diffCard(session, choices)
            );

            builder.Prompts.choice(session, reply, choices);
        } else next();
    },

    function(session, args, next) {
        session.dialogData.skillLevel =
            session.dialogData.skillLevel || skillLevels[args.response.index];

        if (!session.dialogData.duration) {
            durationPrompt.prompt(
                session,
                answers.formatedRandomAnswer("learn.time", [
                    session.userData.name
                ])
            );
        } else next();
    },

    function(session, args, next) {
        session.dialogData.duration =
            session.dialogData.duration || args.response;

        session.send(
            answers.formatedRandomAnswer("learn.fetching", [
                session.userData.name
            ])
        );

        setTimeout(() => next(), 500);
    },

    function(session, args, next) {
        const suggestions = client.querySuggestions(
            session.dialogData.topic,
            session.dialogData.duration,
            session.dialogData.skillLevel
        );

        suggestionStore.addHistory(suggestions, session.userData);

        session.send(
            answers.formatedRandomAnswer("learn.fetched", [
                session.userData.name
            ])
        );

        session.replaceDialog("suggestions", suggestions);
        session.endDialog();
    }
];

const getSkillLevel = entities => {
    var skillLevel = luisUtils.findBestMatch(entities, [
        "skillLevel::low",
        "skillLevel::medium",
        "skillLevel::high"
    ]);

    if (skillLevel) return skillLevel.type.replace("skillLevel::", "");
    return null;
};

const getTopic = entities => {
    var topic = builder.EntityRecognizer.findEntity(entities, "topic");
    if (topic) return topic.entity;
    return null;
};

const getDuration = entities => {
    var duration = builder.EntityRecognizer.findEntity(
        entities,
        "builtin.datetimeV2.duration"
    );

    if (duration && duration.resolution.values.length > 0)
        return duration.resolution.values[0].value;
    return null;
};
const diffCard = (session, choices) => {
    return new builder.HeroCard(session)
        .text(
            answers.formatedRandomAnswer("learn.skill", [
                session.userData.name,
                session.dialogData.topic
            ])
        )
        .buttons([
            builder.CardAction.postBack(session, choices[0], choices[0]),
            builder.CardAction.postBack(session, choices[1], choices[1]),
            builder.CardAction.postBack(session, choices[2], choices[2])
        ]);
};
