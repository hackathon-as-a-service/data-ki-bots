var builder = require("botbuilder");
var answers = require("../speech/answers");

module.exports = [
    (session, args, next) => {
        builder.Prompts.text(
            session,
            answers.formatedRandomAnswer("name.question", [])
        );
    },
    (session, results) => {
        session.userData.name = results.response;
        session.send(
            answers.formatedRandomAnswer("name.update", [session.userData.name])
        );
        session.endDialog();
    }
];
