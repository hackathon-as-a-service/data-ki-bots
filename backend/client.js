var request = require("sync-request");

//const HOST = "https://evil-octopus-34.localtunnel.me/";
const HOST = "https://plastic-cheetah-98.localtunnel.me/";

var querySuggestions = (keyword, read_time, difficulty) => {
    var url = `${HOST}suggestions?keyword=${normalize(
        keyword
    )}&difficulty=${normalize(difficulty)}&read_time=${normalize(read_time)}`;

    data = fetchJsonSync(url);
    console.log(data);
    //Normalize data
    if (data)
        data = data
            .map(s => {
                return {
                    title: s.title,
                    url: s.url,
                    topic: keyword
                };
            })
            .filter(s => s.url);

    return data;
};

var normalize = s => {
    while (s.includes(" ")) s = s.replace(" ", "-");
    return s;
};

var fetchJsonSync = url => {
    let res = request("GET", url);

    try {
        let body = res.getBody("utf8");
        return JSON.parse(body);
    } catch (error) {
        return null;
    }
};

module.exports = { querySuggestions };
