var getRecentTopics = (count, userData) => {
    if (!userData.recentQueries) userData.recentQueries = [];

    return userData.recentQueries
        .map(q => q.topic)
        .filter((v, i, s) => s.indexOf(v) === i)
        .slice(0, count);
};

var getWithTopic = (count, topic, userData) => {
    return userData.recentQueries
        .filter(s => s.topic === topic)
        .slice(0, count);
};

var getRecentQueries = (count, userData) => {
    if (!userData.recentQueries) userData.recentQueries = [];
    return userData.recentQueries.slice(0, count);
};

var addHistory = (suggestions, userData) => {
    if (!userData.recentQueries) userData.recentQueries = [];
    userData.recentQueries = suggestions
        .concat(userData.recentQueries)
        .slice(0, 150);
};

var getSuggestionByUrl = (url, userData) => {
    if (!userData.recentQueries) userData.recentQueries = [];
    return userData.recentQueries.find(value => value.url == url);
};

var addQuery = (query, userData) => {
    if (!userData.recentQueries) userData.recentQueries = [];
    userData.recentQueries = userData.recentQueries
        .concat(suggestions)
        .slice(0, 150);
};

var setAnalytic = (url, clicked, rating, userData) => {
    if (!userData.recentQueries) userData.recentQueries = [];
    let suggestion = getSuggestionByUrl(url, userData);

    if (!suggestion) return;
    suggestion.clicked = clicked;
    suggestion.rating = rating;
};

module.exports = {
    getRecentQueries,
    getRecentTopics,
    getWithTopic,
    setAnalytic,
    addHistory,
    getSuggestionByUrl
};
