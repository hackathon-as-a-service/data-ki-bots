const restify = require("restify");
const builder = require("botbuilder");
const botbuilder_azure = require("botbuilder-azure");
const suggestionStore = require("./store/suggestion");

const rootDialog = require("./dialogs/root");

const durationPrompt = require("./prompts/durationPrompt");
const topicPrompt = require("./prompts/topicPrompt");

// Setup Restify Server
let server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log("%s listening to %s", server.name, server.url);
});

// Create chat connector for communicating with the Bot Framework Service
let connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadata: process.env.BotOpenIdMetadata
});

// Listen for messages from users
server.post("/api/messages", connector.listen());

let bot = new builder.UniversalBot(connector);
server.post("/api/messages", connector.listen());

/*
* LUIS
*/

// Make sure you add code to validate these fields
const luisAppId = "e7fcc4d6-8558-431e-a697-5e96e57e8c2d";
const luisAPIKey = "db7d02f854ec409c9380c3c821c93123";
const luisAPIHostName =
    process.env.LuisAPIHostName || "westus.api.cognitive.microsoft.com";

const LuisModelUrl =
    "https://" +
    luisAPIHostName +
    "/luis/v2.0/apps/" +
    luisAppId +
    "?subscription-key=" +
    luisAPIKey;

//Create a recognizer that gets intents from LUIS, and add it to the bot
const recognizer = new builder.LuisRecognizer(LuisModelUrl);
bot.recognizer(recognizer);

/*
* Prompts
*/
durationPrompt.setup(bot, recognizer);
topicPrompt.setup(bot, recognizer);

/*
* Dialogs 
*/
bot.dialog("/", rootDialog.dialog(recognizer));

bot.dialog("delete", require("./dialogs/delete"));
bot.dialog("continue", require("./dialogs/continue"));
bot.dialog("suggestions", require("./dialogs/suggestions"));
bot.dialog("name", require("./dialogs/name"));
bot.dialog("vote", require("./dialogs/vote"));
bot.dialog("welcome", require("./dialogs/welcome"));
bot.dialog("settings", require("./dialogs/settings"));
bot.dialog("learning", require("./dialogs/learning"));
bot.dialog("recent", require("./dialogs/recent"));
bot.dialog("greet", require("./dialogs/greet"));

bot.on("conversationUpdate", activity => {
    if (activity.membersAdded) {
        activity.membersAdded.forEach(identity => {
            // say hello only when bot joins and not when user joins
            if (identity.id === activity.address.bot.id) {
                var session = bot.loadSession(
                    activity.address,
                    (error, session) => {
                        bot.beginDialog(activity.address, "welcome");
                    }
                );
            }
        });
    }
});

var base64 = require("base-64");
server.get("/redirect/:address/:url", (req, res, next) => {
    try {
        var address = base64.decode(req.params.address);
        var url = base64.decode(req.params.url);
    } catch (ex) {
        console.log("Not base64 encoded!");
    }
    if (url && address) {
        address = JSON.parse(address);

        bot.loadSession(address, (error, session) => {
            let suggestion = suggestionStore.getSuggestionByUrl(
                url,
                session.userData
            );

            suggestionStore.setAnalytic(
                address,
                true,
                suggestion.rating,
                session.userData
            );

            session.userData.currentArticle = suggestion;
        });

        res.setHeader("Content-Type", "text/html");
        res.setHeader("Location", url);
        res.writeHead(307);
        res.end();
        next();
    }
});
